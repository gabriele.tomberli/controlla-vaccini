

const one = 1 // eslint-disable-line no-unused-vars
	, axios = require( 'axios' )
	, lodash = require( 'lodash' )
	, fs = require( 'fs-extra' )
	, path = require( 'path' )
	, mri = require( 'mri' )
	, chalk = require( 'chalk' )
	, console = require( 'console' )
	, beeper = require( 'beeper' )
	, ora = require( 'ora' )
	;


const apiUrl = "https://prenotavaccino.sanita.toscana.it/api/services";
const filepath = path.join( '.', 'data.json' );

const mriOptions = {
	boolean: [ 'dummyData' ],
	number: [ 'delay' ],
}

const sleep = (milliseconds) => {
	return new Promise(resolve => setTimeout(resolve, milliseconds));
}

const currentTime = () => new Date().getTime();

let spinner = ora();

async function start( argz ) {

	const args = mri( argz, mriOptions );

	spinner.spinner = 'dots';
	spinner.prefixText = new Date().toLocaleTimeString();
	spinner.start( 'In esecuzione ...' );

	const makeRequest = async () => {
		if ( args.dummyData ) {
			spinner.text = chalk.cyan( 'Uso i dati di test ...' );
			await sleep( 1000 );
			req = fs.readFile(path.join( '.', 'dummy.json' ), 'utf8').then(JSON.parse).then(data => ({ data, status: 200 }));
		} else {
			spinner.text = chalk.cyan( 'Controllo nuovi dati ...' );
			await sleep( 1000 );
			req = axios.get( apiUrl );
		}
		return req;
	}

	const oldData = await fs.readFile(filepath, 'utf8').then(txt => JSON.parse(txt), e => {
		if (e.code === 'ENOENT')
			return null;
		throw e;
	});

	const request = await makeRequest();

	if ( request.status !== 200 ) {
		spinner.fail( 'Richiesta fallita con codice ' + request.status );
	} else {
		const newData = request.data;

		const oldDict = lodash.keyBy( oldData, 'id' );
		const newDict = lodash.keyBy( newData, 'id' );

		if ( lodash.isEqual( oldDict, newDict ) ) {
			spinner.succeed( chalk.yellow( 'Nessun cambiamento rilevato.' ) );
		} else {
			// console.info( 'Data is different: ', newData );

			if ( !args.dummyData )
				await fs.writeFile( filepath, JSON.stringify( newData ), 'utf8' );

			const keys = lodash( newDict ).keys().concat( lodash.keys( oldDict ) ).uniq().value();
			const result = lodash.map( keys, k => {
				const a = oldDict[k], b = newDict[k];
				if ( !oldDict[k] ) {
					const header = chalk.green( '# Nuova categoria trovata: ' + k + '\n' );
					const data = lodash( b )
						.keys()
						.filter( key => key !== 'id' )
						.filter( key => key !== 'message' )
						.filter( key => key !== 'recallDays' )
						.map( key => chalk.green( `  - ${key}: ${b[key]}` ) )
						.value();
					return header + data.join( '\n' );
				} else if ( !newDict[k] ) {
					const header = chalk.red( '# Categoria rimossa: ' + k + '\n' );
					const data = lodash( a )
						.keys()
						.filter( key => key !== 'id' )
						.filter( key => key !== 'message' )
						.filter( key => key !== 'recallDays' )
						.map( key => chalk.red( `  - ${key}: ${a[key]}` ) )
						.value();
					return header + data.join( '\n' );
				} else if ( lodash.isEqual( oldDict[k], newDict[k] ) ) {
					const header = chalk.yellow( '# Nessuna modifica per: ' + k + '\n' );
					const data = lodash( a )
						.keys()
						.filter( key => key !== 'id' )
						.filter( key => key !== 'message' )
						.filter( key => key !== 'recallDays' )
						.map( key => chalk.yellow( `  - ${key}: ${a[key]}` ) )
						.value();
					return header + data.join( '\n' );
				} else {
					
					const header = chalk.cyan( '# Riscontrata modifica per ' + k + ':\n' );
					
					const diffs = lodash( b )
						.keys()
						.filter( key => !lodash.isEqual( a[key], b[key] ) )
						.map( key => chalk.cyan( `  - ${key}: ${a[key]} ==> ${b[key]}` ) )
						.value()
					const eqs = lodash( b )
						.keys()
						.filter( key => key !== 'id' )
						.filter( key => key !== 'message' )
						.filter( key => key !== 'recallDays' )
						.filter( key => lodash.isEqual( a[key], b[key] ) )
						.map( key => `  - ${key}: ${a[key]}` )
						.value();

					return header + eqs.concat( diffs ).join( '\n' );
				}

			});

			spinner.succeed( chalk.cyan( 'Cambiamenti rilevati: ' ) );
			console.info( result.join('\n') );
			await beeper("*----*");

		}

	}

	if ( args.delay ) {
		spinner.text = 'Prossima esecuzione alle ' + new Date( currentTime() + args.delay * 1000 ).toLocaleTimeString() + ' ...';
		spinner.spinner = 'bounce';
		spinner.prefixText = new Date().toLocaleTimeString();
		spinner.start();
		await sleep( args.delay * 1000 );
		start( argz );
	}

}

start( process.argv.slice( 2 ) ).catch( console.error );
